# test_field_validator.py

import pytest
from unittest import TestCase

## Standard Lib
import logging

## Formulator
from formulator.meta import Node
from formulator.validation import *

class A(Node):
    parent = SizedRegexString(maxlen=20, pattern=r"^[^0-9]")
    child = SizedString(maxlen=20)
    edges = NonNegativeInteger()

class B(Node):
    ingress = NonNegativeInteger()
    egress = NonNegativeInteger()
    payload = SizedRegexString(maxlen=20, pattern=r"[A-Za-z_ ]+$")


def floatAdd(arg1: NegativeInteger, arg2: Negative, arg3: str):
    arg1 = arg1
    arg2 = arg2
    arg3 = arg3
    return arg1 + arg2 + 0.5


def negativeFloatAdd(arg1: NegativeInteger, arg2: Negative) -> Negative:
    arg1 = arg1
    arg2 = arg2
    return arg1 + arg2 + 0.5

class TryTesting(TestCase):
    def test_create_node_children(self):
        logging.basicConfig(filename='./formulator.log',
                            level=logging.DEBUG, format="%(asctime)s %(message)s", datefmt='%m/%d/%Y %I:%M:%S %p',
                            filemode='w')
        logging.getLogger('ttils').addHandler(logging.NullHandler())
        logging.logThreads = 0
        logging.logProcesses = 0
        logging._srcfile = None
        a = A(parent="Apar", edges=2, child="Node D")
        b = B(100, 24, "This is the payload")


class AnnotationValidation(TestCase):
    def test_identifies_validator_fields(self):
        floatAdd(-5, -1.5, "some arg")
    
    def test_identifies_return_field(self):
        negativeFloatAdd(1,5)
